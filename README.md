# 简单的ci测试

## 前期准备
首先要有一个runner，提供ci运行的环境，runner有special-runner和shared-runner两种

## 安装 special-runner: linux平台
```bash
# 下载gitlab-runner放到/usr/local/bin 下
sudo curl -L --output /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64

# 给gitlab-runner添加执行权限
sudo chmod +x /usr/local/bin/gitlab-runner

# 创建一个gitlab-runner用户
sudo useradd --comment 'GitLab Runner' --create-home gitlab-runner --shell /bin/bash

# 安装并开启服务
sudo gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner
sudo gitlab-runner start
```
## 注册 runner- linux平台
```bash
# 注册 (本地注册和docker注册不一样)
docker run --rm -it -v /srv/gitlab-runner/config:/etc/gitlab-runner gitlab/gitlab-runner register

# 根据终端提示输入URL和token

# 输入tag
```
## demo
### 编译
### 运行自动化测试
### 发邮件 